#!/bin/sh

#script require these tools:
# adb, bundletool-all-1.4.0.jar ( place in your home folder ), java

# I just test with linux evironment
# Please place this script in your root project and make sure it have execute permission

# Declare variable
RN_PROJECT_ROOT=$(pwd)
PACKAGE_NAME="com.example.app"
PATH_TO_KEYSTORE=$RN_PROJECT_ROOT/android/app/keystore.keystore
KEYSTORE_PASSWORD=keystore_password
KEYSTORE_ALIAS=key_alias

npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle
adb uninstall $PACKAGE_NAME
cd $RN_PROJECT_ROOT/android && ./gradlew clean && ./gradlew bundleRelease
rm $HOME/apks.apks
rm $HOME/app-release.aab
cp $RN_PROJECT_ROOT/android/app/build/outputs/bundle/release/app-release.aab $HOME
java -jar $HOME/bundletool-all-1.5.0.jar build-apks --bundle=$HOME/app-release.aab --output=$HOME/apks.apks --ks=$PATH_TO_KEYSTORE --ks-pass=pass:$KEYSTORE_PASSWORD --ks-key-alias=$KEYSTORE_ALIAS --key-pass=pass:$KEYSTORE_PASSWORD
java -jar $HOME/bundletool-all-1.5.0.jar install-apks --apks=$HOME/apks.apks
adb shell am start -n $PACKAGE_NAME/$PACKAGE_NAME.MainActivity

# in your device will appear your app, if it work fine, you can upload app-release.aab file to google play store 😉