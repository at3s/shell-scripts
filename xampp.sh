#!/bin/sh

isRoot=$(whoami)

start() {
  echo 'starting...'
  /opt/lampp/manager-linux-x64.run start
  echo 'done'
}

restart() {
  echo 'restarting...'
  /opt/lampp/manager-linux-x64.run restart
  echo 'done'
}

stop() {
  echo 'stopping...'
  /opt/lampp/manager-linux-x64.run stop
  echo 'done'
}

main() {
  if [ "$isRoot" != "root" ]; then
    echo '**********************************'
    echo 'Please run this script with root user...'
  else
    if [ "$1" = "start" ]; then
      start
      exit
    fi
    #
    if [ "$1" = "restart" ]; then
      restart
      exit
    fi
    #
    if [ "$1" = "stop" ]; then
      stop
      exit
    fi

    echo 'use: sh xampp.sh [start|restart|stop]'

  fi
}

main $*