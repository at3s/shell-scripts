#!/bin/sh

isRoot=$(whoami)

Make() {
    make
    if [ $? -eq 0 ]
    then
        make install
        if [ $? -eq 0 ]
        then
            echo '**********************************'
            echo 'Success'
            echo 'If Vmware still error..pls reboot your machine.'
        else
            echo '**********************************'
            echo 'An error occured'
        fi
    else
        apt-get install linux-headers-$(uname -r)
        Make
    fi
}

Fix() {
    echo '**********************************'
    echo 'fixing.....'
    DIR="/opt/vmware-host-modules/"
    if [ -d "$DIR" ]
    then
        rm -rf "$DIR"
    fi
    git clone -b workstation-$( grep player.product.version /etc/vmware/config | sed '/.*\"\(.*\)\".*/ s//\1/g' ) https://github.com/mkubecek/vmware-host-modules.git /opt/vmware-host-modules/
    if [ $? -eq 0 ]
    then
        cd /opt/vmware-host-modules/
        Make
    else
        echo '**********************************'
        echo 'failed'
    fi
}

if [ "$isRoot" != "root" ]
then
    echo '**********************************'
    echo 'Please run this script with root user...'
else
    Fix
fi